gruntFunction = (grunt)->

  gruntConfig =
    pkg:
      grunt.file.readJSON('package.json')

# STILUS ###################################
    stylus:
      compile:
        files:[
          expand: true
          cwd: "resources/stylus"
          src: "**/*.styl"
          dest: "public/css"
          ext: ".css"
        ]

# JADE #####################################
    jade:
      views:
        options:
          pretty: true
          data:
            debug: false
        files:[
          expand: true
          cwd: "resources/jade"
          src: ["**/*.jade"]
          dest: "resources/views"
          ext: ".blade.php"
        ]
      templates:
        options:
          pretty: true
          data:
            debug: false
        files: [
          expand: true
          cwd: "resources/app"
          src: "**/*.jade"
          dest: "resources/app"
          ext: ".hbs"
        ]

# HANDLEBARS ################################
    handlebars:
      compile:
        options:
          namespace: false
          amd: true
        files:[
          expand: true
          cwd: "resources/app/"
          src: "**/*.hbs"
          dest: "public/js/app"
          ext: ".js"
        ]

# COFFESCRIPT ###############################
    coffee:
      glob_to_multiple:
        expand: true
        cwd: 'resources/app'
        src: '**/*.coffee'
        dest: 'public/js/app'
        ext: '.js'

# CLEAN #####################################
    clean:
      hbs: ["resources/app/**/*.hbs"]

# WATCH #####################################
    watch:
      stylus:
        files: ['resources/stylus/**/*.styl']
        tasks: ['stylus']
      views:
        files: ['resources/jade/**/*.jade']
        tasks: ['jade:views']
      templates:
        files: ['resources/app/**/*.jade']
        tasks: ['jade:templates', 'handlebars', 'clean']
      coffee:
        files: ['resources/app/**/*.coffee']
        tasks: ['coffee']

# BrowserSync ##############################
    browserSync:
      bsFiles:
        src : ['public/**/*', 'resources/views/**/*']
      options:
        watchTask: true
        proxy: 'localhost:8000'

  # LOAD TASKS #################################
  grunt.loadNpmTasks 'grunt-contrib-stylus'
  grunt.loadNpmTasks 'grunt-contrib-uglify'
  grunt.loadNpmTasks 'grunt-contrib-watch'
  grunt.loadNpmTasks 'grunt-contrib-jade'
  grunt.loadNpmTasks 'grunt-contrib-handlebars'
  grunt.loadNpmTasks 'grunt-contrib-clean'
  grunt.loadNpmTasks 'grunt-contrib-coffee'
  grunt.loadNpmTasks 'grunt-browser-sync'

  # LAST SETTINGS #############################
  grunt.registerTask 'default', ['browserSync', 'watch']
  grunt.initConfig gruntConfig


module.exports = gruntFunction