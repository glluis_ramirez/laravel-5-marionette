## Laravel 5 y MarionetteJS template

Este repositorio es un template para hacer aplicaciones basadas en Laravel 5, MarionetteJS y RequireJS.

## Descripcion del template
Este template cuenta con una instalacion de laravel 5 en blanco.
Un archivo Gruntfile para automatizar tareas relacionadas con jade, coffeescript, stylus, handlebars y livereload.
Un archivo package.json con las dependencias que utiliza Grunt
Un archivo bower.json con las dependencias basicas JS que utiliza un proyecto de MarionetteJS

## Pasos para utilizar el template
* Descargar el repositorio
* Ejecutar "composer update" para descargar los archivos necesarios de la carpeta 'vendor'
* Ejecutar "npm install" para descargar las dependencias utilizadas por grunt
* Ejecutar "bower install" para descargar las dependencias de la aplicacion JS
* Ejecutar "grunt" para empezar a trabajar en el proyecto

## Javascript
Este template esta configurado para trabajar con archivos de coffeescript, para que los archivos de coffeescript sean conviertan en archivos de javascript, grunt tiene que estar ejecutandose y los archivos coffeescript tienen que estar en el directorio resources/app.
Grunt coloca los archivos javascipt dentro de la carpeta public/js/app.
  
## Stylus
Los archivos de stylus son procesados a formato css de forma automatica por grunt cuando se encuentran en la carpeta resources/stylus y son colocados en la carpeta public/css una vez que son convertidos a CSS.

## Vistas
Las vistas procesadas por laravel se deben de crear en la carpeta resources/jade una vez que grunt las procesa se encontran en resources/views con extension .blade.php
 
## Templates
Los templates al ser utilizados por la aplicacion de javascript se deberan ubicar dentro de la carpeta resources/app en formato JADE, no es necesario que esten en la raiz de esta carpeta grunt esta configurado para encontrar cualquier archivo jade convertirlo a handlebars y compilarlo en cualquier subcarpeta de resources/app.

