dependencies = ['marionette']

define dependencies, (Mn)->
  class App extends Mn.Application
    onStart: ->
      console.log 'Require.js and MarionetteJS Works!!'
  return App