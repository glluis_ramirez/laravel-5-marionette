#Helpers to use Timers and Invervals with coffescript easier
window.delay = (ms, fn)-> setTimeout(fn, ms)
window.timer = (ms, fn)-> setInterval(fn, ms)

#Requirejs custom configurations
requirejs.config
  baseUrl: 'js'
  paths:
    'backbone':             'vendor/backbone/backbone'
    'backbone.babysitter':  'vendor/backbone.babysitter/lib/backbone.babysitter'
    'backbone.radio':       'vendor/backbone.radio/build/backbone.radio'
    'backbone.wreqr':       'vendor/backbone.wreqr/lib/backbone.wreqr'
    'handlebars':           'vendor/handlebars/handlebars.runtime.amd'
    'jquery':               'vendor/jquery/dist/jquery'
    'marionette':           'vendor/marionette/lib/backbone.marionette'
    'underscore':           'vendor/underscore/underscore'

#dependencies used by the application
dependencies = ['jquery', 'app/app/app']

#Kick-starting the application
require dependencies, ($, App)->

  $(document).ready ->
    app = new App()
    app.start()